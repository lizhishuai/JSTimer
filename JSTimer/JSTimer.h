//
//  JSTimer.h
//  JSTimer
//
//  Created by JasonLee on 2020/3/3.
//  Copyright © 2020 JasonLee. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^ JSTimerBlock)(void);
@interface JSTimer : NSObject
//默认开启
+(JSTimer *)scheduleTimerWithInterval:(NSTimeInterval)interval
                               repeat:(BOOL)repeat
                               target:(id)target
                             selector:(SEL)selector;
+(JSTimer *)scheduleTimerWithInterval:(NSTimeInterval)interval
                               repeat:(BOOL)repeat
                                block:(JSTimerBlock)block;
//默认关闭需调用fire方法
+(JSTimer *)timerWithInterval:(NSTimeInterval)interval
                       repeat:(BOOL)repeat
                       target:(id)target
                     selector:(SEL)selector;
+(JSTimer *)timerWithInterval:(NSTimeInterval)interval
                       repeat:(BOOL)repeat
                        block:(JSTimerBlock)block;
/**
 开始
 */
-(void)fire;
/**
暂停
*/
-(void)pause;
/**
恢复
*/
-(void)resume;

/**
销毁
*/
-(void)invalidate;
@end

NS_ASSUME_NONNULL_END
