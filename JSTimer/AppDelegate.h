//
//  AppDelegate.h
//  JSTimer
//
//  Created by JasonLee on 2020/3/3.
//  Copyright © 2020 JasonLee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;
@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

