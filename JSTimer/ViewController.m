//
//  ViewController.m
//  JSTimer
//
//  Created by JasonLee on 2020/3/3.
//  Copyright © 2020 JasonLee. All rights reserved.
//

#import "ViewController.h"
#import "JSTimer.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    JSTimer *timer = [JSTimer scheduleTimerWithInterval:1 repeat:YES block:^{
        NSLog(@"========");
    }];
    [timer fire];
//    [timer pause];
//    [timer pause];
//    JSTimer *timer1 = [JSTimer scheduleTimerWithInterval:1 repeat:NO target:self selector:@selector(timerAction)];
//    JSTimer *timer2 = [JSTimer timerWithInterval:1 repeat:YES block:^{
//        NSLog(@"++++++");
//    }];
//    [timer2 fire];
//    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:1 repeats:NO block:^(NSTimer * _Nonnull timer) {
//        NSLog(@"timer");
//    }];
//    NSTimer *timer2 = [NSTimer scheduledTimerWithTimeInterval:1 repeats:YES block:^(NSTimer * _Nonnull timer) {
//        NSLog(@"timer2");
//    }];
//    [timer2 fire];
//    NSTimer *timer1 = [NSTimer timerWithTimeInterval:1 repeats:YES block:^(NSTimer * _Nonnull timer) {
//        NSLog(@"timer1");
//    }];
//    [timer1 fire];
    // Do any additional setup after loading the view.
}
-(void)timerAction{
    NSLog(@"----------");
}

@end
